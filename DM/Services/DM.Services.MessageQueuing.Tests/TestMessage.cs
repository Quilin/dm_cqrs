using System;

namespace DM.Services.MessageQueuing.Tests
{
    public class TestMessage
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
    }
}